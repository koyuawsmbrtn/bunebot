#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import telegram
import logging
from telegram.error import NetworkError, Unauthorized
import os
import random

if 'TOKEN' in os.environ:
    TOKEN = os.environ.get('TOKEN')
else:
    TOKEN = ""

print("Listening...")

blacklist = ""
counter = 0

def handle(bot):
    global update_id
    global blacklist
    global counter
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        if update.effective_message:
            chat_id = update.effective_message.chat_id
            chat_type = update.effective_message.chat.type
            if update.effective_message.text:
                if update.effective_message["text"].startswith("/bun"):
                    bunes = os.listdir("bunes")
                    bunes.pop(0)
                    rnd = random.randint(0, len(bunes) - 1)
                    if len(blacklist.split(",")) > 99: # if the blacklist overflows for whatever reason
                        counter = 0
                        blacklist = ""
                    indexes = len(bunes) - 1
                    while str(rnd) in blacklist and counter <= indexes:
                        rnd = random.randint(0, len(bunes) - 1)
                    f = open("bunes/"+bunes[rnd], "rb")
                    bot.sendPhoto(chat_id, f, reply_to_message_id=update.effective_message.message_id)
                    f.close()
                    blacklist = str(rnd) + blacklist + ","
                    if counter > 25:
                        blacklist = ""
                        counter = 0
                    counter = counter+1
                if update.effective_message["text"].startswith("/start") and chat_type == "private":
                    bot.sendMessage(chat_id, "👋")

global update_id
bot = telegram.Bot(TOKEN)
try:
    update_id = bot.get_updates()[0].update_id
except IndexError:
    update_id = None

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

while True:
    try:
        handle(bot)
    except NetworkError:
        time.sleep(1)
    except Unauthorized:
        update_id += 1